export default {
  namespaced: true,

  state: {
    centers: [
      {
        id: "vilnius-a",
        name: "Vilnius A",
        description: "This is Vilnius A description",
        resources: [
          {
            date: "2021-06-15",
            assets: [
              { vaccineId: "pfizer", recipientId: null },
              { vaccineId: "astrazeneca", recipientId: null },
            ],
          },
          {
            date: "2021-06-16",
            assets: [{ vaccineId: "pfizer", recipientId: null }],
          },
        ],
      },
      {
        id: "kaunas-a",
        name: "kaunas A",
        description: "This is Kaunas A description",
        resources: [
          {
            date: "2021-06-15",
            assets: [
              { vaccineId: "pfizer", recipientId: null },
              { vaccineId: "astrazeneca", recipientId: null },
            ],
          },
          {
            date: "2021-06-17",
            assets: [{ vaccineId: "astrazeneca", recipientId: null }],
          },
        ],
      },
    ],
  },

  getters: {
    centersCount: (state) => {
      return state.centers.length;
    }
  },

  mutations: {},

  actions: {},
};