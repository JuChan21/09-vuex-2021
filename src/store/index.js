import Vue from 'vue'
import Vuex from 'vuex'
import centers from './modules/centers'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    centers
  }
});